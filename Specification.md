# Common

{
    "type": "(null|boolean|string|number|array|object)",
    "required": true|false,
}

# Null

{
    "type": "null"
    "required": true|false,
}

# Boolean

{
    "type": "boolean"
    "required": true|false,
    "value": true|false
}

# String

{
    "type": "string"
    "required": true|false,
    "regexp": ".*",
    "min": 1,
    "max": 100
}

# Number

{
    "type": "number"
    "required": true|false,
    "floatingPoint": true|false,
    "min": 0.0,
    "max": 100.0
}

# Array

{
    "type": "array"
    "required": true|false,
    "min": 1,
    "max": 100,
    "items": "(file://subSchema.json|../subSchema)" | {}
}

# Object

{
    "type": "object"
    "required": true|false,
    "properties": {
        "a": "(file://subSchema.json|../subSchema)" | {}
    }
}

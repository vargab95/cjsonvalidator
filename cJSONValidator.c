#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "cJSON.h"
#include "cJSONValidator.h"

static cJSON *readFromFile(const char *path);
static cJSONValidator *processSchemaObject(cJSON *schemaJSON);
static cJSONValidator *processReferenceString(const char *string);

cJSONValidator *cJSONValidator_LoadSchema(const char *path)
{
    cJSON *schemaJSON;
    cJSONValidator *schema;

    schemaJSON = readFromFile(path);
    if (schemaJSON == NULL)
    {
        return NULL;
    }

    schema = calloc(1, sizeof(cJSONValidator));
    if (schema == NULL)
    {
        return NULL;
    }

    return NULL;
}

static cJSON *readFromFile(const char *path)
{
    char *buffer;
    size_t size, read;
    FILE *file;
    cJSON *json;

    file = fopen(path, "r");
    if (!file)
    {
        return NULL;
    }

    fseek(file, 0, SEEK_END);
    size = ftell(file);
    fseek(file, 0, SEEK_SET);

    buffer = malloc(size);
    if (buffer == NULL)
    {
        fclose(file);
        return NULL;
    }

    read = fread(buffer, sizeof(char), size, file);
    if (read != size)
    {
        free(buffer);
        fclose(file);
        return NULL;
    }

    json = cJSON_Parse(buffer);

    free(buffer);
    fclose(file);

    return json;
}

static cJSONValidator *processSchemaObject(cJSON *schemaJSON)
{
    cJSON *temp;
    cJSONValidator *schemaElement;
    cJSONValidator_Types type;
    char *typeString;

    schemaElement = calloc(1, sizeof(cJSONValidator));
    if (schemaElement == NULL)
    {
        return NULL;
    }

    typeString = cJSON_GetStringValue(cJSON_GetObjectItemCaseSensitive(schemaJSON, "type"));
    if (typeString == NULL)
    {
        free(schemaElement);
        return NULL;
    }

    if (strcmp(typeString, "null") == 0)
    {
        type = cJSONValidator_Null;
    }
    else if (strcmp(typeString, "boolean") == 0)
    {
        type = cJSONValidator_Boolean;
    }
    else if (strcmp(typeString, "string") == 0)
    {
        type = cJSONValidator_String;
    }
    else if (strcmp(typeString, "number") == 0)
    {
        type = cJSONValidator_Number;
    }
    else if (strcmp(typeString, "array") == 0)
    {
        type = cJSONValidator_Array;
    }
    else if (strcmp(typeString, "object") == 0)
    {
        type = cJSONValidator_Object;
    }
    else
    {
        free(schemaElement);
        return NULL;
    }

    temp = cJSON_GetObjectItemCaseSensitive(schemaJSON, "required");
    if (temp)
    {
        if (!cJSON_IsBool(temp))
        {
            free(schemaElement);
            return NULL;
        }

        schemaElement->required = temp->valueint;
    }
    else
    {
        schemaElement->required = false;
    }

    switch (type)
    {
    case cJSONValidator_Null:
        break;
    case cJSONValidator_Boolean:
        temp = cJSON_GetObjectItemCaseSensitive(schemaJSON, "value");
        if (temp)
        {
            if (!cJSON_IsBool(temp))
            {
                free(schemaElement);
                return NULL;
            }

            schemaElement->rule.boolean.valueSpecified = true;
            schemaElement->rule.boolean.value = temp->valueint;
        }
        else
        {
            schemaElement->rule.boolean.valueSpecified = false;
        }
        break;
    case cJSONValidator_String:
        temp = cJSON_GetObjectItemCaseSensitive(schemaJSON, "regexp");
        if (temp)
        {
            schemaElement->rule.string.regexp = cJSON_GetStringValue(temp);
            if (schemaElement->rule.string.regexp == NULL)
            {
                free(schemaElement);
                return NULL;
            }
        }
        else
        {
            schemaElement->rule.string.regexp = NULL;
        }

        temp = cJSON_GetObjectItemCaseSensitive(schemaJSON, "min");
        if (temp)
        {
            if (!cJSON_IsNumber(temp))
            {
                free(schemaElement);
                return NULL;
            }

            schemaElement->rule.string.minSpecified = true;
            schemaElement->rule.string.min = cJSON_GetNumberValue(temp);
        }
        else
        {
            schemaElement->rule.string.minSpecified = false;
        }

        temp = cJSON_GetObjectItemCaseSensitive(schemaJSON, "max");
        if (temp)
        {
            if (!cJSON_IsNumber(temp))
            {
                free(schemaElement);
                return NULL;
            }

            schemaElement->rule.string.maxSpecified = true;
            schemaElement->rule.string.max = cJSON_GetNumberValue(temp);
        }
        else
        {
            schemaElement->rule.string.maxSpecified = false;
        }
        break;
    case cJSONValidator_Number:
        temp = cJSON_GetObjectItemCaseSensitive(schemaJSON, "floatingPoint");
        if (temp)
        {
            if (!cJSON_IsBool(temp))
            {
                free(schemaElement);
                return NULL;
            }

            schemaElement->rule.number.type = temp->valueint ? cJSONValidator_Double : cJSONValidator_Integer;
        }
        else
        {
            schemaElement->rule.number.type = cJSONValidator_Double;
        }

        temp = cJSON_GetObjectItemCaseSensitive(schemaJSON, "min");
        if (temp)
        {
            if (!cJSON_IsNumber(temp))
            {
                free(schemaElement);
                return NULL;
            }

            schemaElement->rule.number.minSpecified = true;
            schemaElement->rule.number.min = cJSON_GetNumberValue(temp);
        }
        else
        {
            schemaElement->rule.number.minSpecified = false;
        }

        temp = cJSON_GetObjectItemCaseSensitive(schemaJSON, "max");
        if (temp)
        {
            if (!cJSON_IsNumber(temp))
            {
                free(schemaElement);
                return NULL;
            }

            schemaElement->rule.number.maxSpecified = true;
            schemaElement->rule.number.max = cJSON_GetNumberValue(temp);
        }
        else
        {
            schemaElement->rule.number.maxSpecified = false;
        }
        break;

    case cJSONValidator_Array:
        temp = cJSON_GetObjectItemCaseSensitive(schemaJSON, "min");
        if (temp)
        {
            if (!cJSON_IsNumber(temp))
            {
                free(schemaElement);
                return NULL;
            }

            schemaElement->rule.array.minSpecified = true;
            schemaElement->rule.array.min = cJSON_GetNumberValue(temp);
        }
        else
        {
            schemaElement->rule.array.minSpecified = false;
        }

        temp = cJSON_GetObjectItemCaseSensitive(schemaJSON, "max");
        if (temp)
        {
            if (!cJSON_IsNumber(temp))
            {
                free(schemaElement);
                return NULL;
            }

            schemaElement->rule.array.maxSpecified = true;
            schemaElement->rule.array.max = cJSON_GetNumberValue(temp);
        }
        else
        {
            schemaElement->rule.array.maxSpecified = false;
        }

        temp = cJSON_GetObjectItemCaseSensitive(schemaJSON, "items");
        if (temp)
        {
            if (cJSON_IsObject(temp))
            {
                schemaElement->rule.array.items = processSchemaObject(temp);
                if (schemaElement->rule.array.items == NULL)
                {
                    free(schemaElement);
                    return NULL;
                }
            }
            else if (cJSON_IsString(temp))
            {
                schemaElement->rule.array.items = processReferenceString(cJSON_GetStringValue(temp));
                if (schemaElement->rule.array.items == NULL)
                {
                    free(schemaElement);
                    return NULL;
                }
            }
            else
            {
                free(schemaElement);
                return NULL;
            }
        }
        break;

    case cJSONValidator_Object:
        break;
    }

    return schemaElement;
}

static cJSONValidator *processReferenceString(const char *string)
{
    if (strncmp(string, "file://", 7) == 0)
    {
        cJSON *schemaJSON = readFromFile(&string[8]);
        if (schemaJSON == NULL)
        {
            return NULL;
        }

        return processSchemaObject(schemaJSON);
    }

    const char *ptr = &string[8];
    cJSONValidator *temp;
    do
    {
        if (strncmp(ptr, "../", 3) == 0)
        {
            temp = temp->parent;
            ptr += 3;
        }
        else
        {
            cJSONValidator *current = temp;
            while (current)
            {

                current = current->next;
            }
        }
    } while (temp);

    return NULL;
}

bool cJSONValidator_Validate(const cJSONValidator *const schema, const cJSON *const data)
{

    return false;
}

void cJSONValidator_DestroySchema(const cJSONValidator *const schema) {}

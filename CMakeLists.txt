cmake_minimum_required (VERSION 3.5)

project(cJSONValidator VERSION 1.0.0)

include(ExternalProject)
set(EXTERNAL_INSTALL_LOCATION ${CMAKE_BINARY_DIR}/external)
ExternalProject_Add(cJSON
    GIT_REPOSITORY https://github.com/DaveGamble/cJSON.git
    GIT_TAG v1.7.15
    CMAKE_ARGS -DCMAKE_INSTALL_PREFIX=${EXTERNAL_INSTALL_LOCATION}
)

include_directories(${EXTERNAL_INSTALL_LOCATION}/include/cjson)
include_directories(${CMAKE_CURRENT_SOURCE_DIR})

set(CMAKE_EXPORT_COMPILE_COMMANDS ON)

add_library (cJSONValidator SHARED ${CMAKE_CURRENT_SOURCE_DIR}/cJSONValidator.c)

install(TARGETS cJSONValidator RUNTIME DESTINATION ${CMAKE_INSTALL_PREFIX}/bin)

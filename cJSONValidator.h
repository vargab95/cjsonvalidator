#ifndef __C_JSON_VALIDATOR_H__
#define __C_JSON_VALIDATOR_H__

#include <stdbool.h>
#include <stdint.h>

#include <cJSON.h>

typedef enum
{
    cJSONValidator_Null,
    cJSONValidator_Boolean,
    cJSONValidator_String,
    cJSONValidator_Number,
    cJSONValidator_Array,
    cJSONValidator_Object,
} cJSONValidator_Types;

typedef enum
{
    cJSONValidator_Integer,
    cJSONValidator_UnsignedInteger,
    cJSONValidator_Double,
} cJSONValidator_NumericTypes;

typedef struct _cJSONValidator
{
    char *key;
    uint8_t required : 1;

    cJSONValidator_Types type;
    union
    {
        struct
        {
        } null;
        struct
        {
            bool valueSpecified;
            bool value;
        } boolean;
        struct
        {
            char *regexp;
            bool minSpecified;
            uint32_t min;
            bool maxSpecified;
            uint32_t max;
        } string;
        struct
        {
            cJSONValidator_NumericTypes type;
            union
            {
                int i;
                unsigned int ui;
                double d;
            } value;

            bool minSpecified;
            double min;
            bool maxSpecified;
            double max;
        } number;
        struct
        {
            bool minSpecified;
            uint32_t min;
            bool maxSpecified;
            uint32_t max;
            struct _cJSONValidator *items;
        } array;
        struct
        {
            struct _cJSONValidator *items;
        } object;
    } rule;

    struct _cJSONValidator *parent;
    struct _cJSONValidator *next;
} cJSONValidator;

cJSONValidator* cJSONValidator_LoadSchema(const char *path);
bool cJSONValidator_Validate(const cJSONValidator * const schema, const cJSON * const data);
void cJSONValidator_DestroySchema(const cJSONValidator * const schema);

#endif
